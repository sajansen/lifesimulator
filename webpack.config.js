const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    mode: 'production',

    entry: './src/index.ts',
    resolve: {
        modules: [
            path.resolve(__dirname, "src/js"),
            path.resolve(__dirname, "src/style"),
            "node_modules"
        ],
        extensions: [".jsx", ".js", ".json", ".ts", ".tsx"],
        alias: {
            '~': path.resolve(__dirname, 'src'),
        }
    },
    module: {
        rules: [
            {
                test: /\.ts|\.tsx$/,
                use: [
                    'awesome-typescript-loader'
                ],
                exclude: /node_modules/
            },
            {
                test: /\.js|\.jsx$/,
                use: [
                    'source-map-loader'
                ],
                exclude: /node_modules/
            },
            {
                test: /\.(css|sass|scss)$/,
                use: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: [{
                            loader: 'css-loader'
                        },
                        {
                            loader: 'sass-loader'
                        }
                    ]
                })
            },
            {
                test: /\.(png|svg|jpg|gif)$/,
                use: [
                    'file-loader'
                ]
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/,
                use: [
                    'file-loader'
                ]
            },
            {
                test: /\.json$/,
                loader: 'json-loader'
            },
            {
                test: /\.html$/,
                use: [ {
                    loader: 'html-loader',
                }],
            }
        ]
    },
    output: {
        filename: 'main.js',
        path: path.resolve(__dirname, 'dist')
    },

    devtool: "source-map",

    plugins: [
        new ExtractTextPlugin("main.css"),
    ],

    performance: {
        hints: false
    },
};
