# Life Simulator
_A simulator of things_

![Screenshot where all objects and one object his view is shown](img/screenshot0.png)

## Setup

Download the the `dist/` and `public/` directories and you're good to go. Fire up [public/index.html](public/index.html) to see the magic happen!

#### Advanced usage

The simulator is controllable via a window references for debug purposes: `window.simulator`. You can run all the corresponding functions through this reference, like `window.simulator.pause()`. Also, you can access anything from this reference, like `window.simulator.objects[0].speed`.



## Develop

### Setup

Run:
```bash
npm run build
```

#### MovingObject

The MovingObject class is the class is it all about. Purpose of this class is to be as independent as possible. Therefore, it requires no arguments except for the Graphics object. Of course, you'll also want to know in which surrounding you are. The Graphics object is used for keeping the MovingObject inside the window. The Graphics' View object is used for the MovingObject to look around. If the graphics object is null or left out, the MovingObject will be blind and ignore all of it's environment (like walls and other objects).

```javascript
const graphics = new Graphics();
const object = new MovingObject(graphics);
```

The MovingObject updates his state every time `MovingObject.step()` is executed. 

##### Goals

Each MovingObject has a goal, because, what else would be the purpose in live? Everyone of us has a goal in live, so do they. The MovingObject tries to reach his goal as fast as possible. When reached, the Simulation will give him a new goal, so he won't be lazy and useless.


### Test

```bash
npm run test
```

Testing is done using Mocha. Webpack builds the tests from the _tests/_ directory to _dist/test.js_, where Mocha can execute them.
