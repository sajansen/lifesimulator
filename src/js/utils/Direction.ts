/**
 * Object to contain a direction.
 * This direction can be limited between 0 up to and excluding 360 degrees.
 * For performance, prefer the use of degrees over radians.
 */
import Coordinate from "./Coordinate";

export default class Direction {
    private _degrees: number = 0;
    private _radians: number = 0;

    constructor(degrees = 0, limit: boolean = true) {
        this.setDegrees(degrees, limit);
    }

    degrees(): number {
        return this._degrees;
    }

    setDegrees(degrees: number, limit: boolean = true) {
        this._degrees = degrees;
        if (limit)
            this._limit();
        this._radians = Direction.degreesToRadian(this._degrees);
    }

    addDegrees(degrees: number, limit: boolean = true) {
        this.setDegrees(this._degrees + degrees, limit);
    }

    radians(): number {
        return this._radians;
    }

    setRadians(radians: number, limit: boolean = true) {
        this.setDegrees(Direction.radiansToDegrees(radians), limit);
    }

    private _limit() {
        if (this._degrees < 0) {
            this._degrees += 360;
        } else if (this._degrees >= 360) {
            this._degrees %= 360;
        }
    }

    static degreesToRadian(degrees: number): number {
        return degrees * Math.PI / 180;
    }

    static radiansToDegrees(radians: number): number {
        return radians * 180 / Math.PI;
    }

    static absDifference(firstDirection: Direction, secondDirection: Direction): Direction {
        if (firstDirection.degrees() > secondDirection.degrees()) {
            return new Direction(firstDirection.degrees() - secondDirection.degrees());
        } else {
            return new Direction(secondDirection.degrees() - firstDirection.degrees());
        }
    }

    isLeftFrom(otherDirection: Direction): boolean {
        if (otherDirection.degrees() > this.degrees()) {
            return otherDirection.degrees() <= this.degrees() + 180;
        } else {
            return otherDirection.degrees() <= this.degrees() - 180;
        }
    }
    
    static getDirectionTo(from: Coordinate, target: Coordinate): Direction {
        const diffX = target.x - from.x;
        const diffY = from.y - target.y;

        let newRadians = Math.atan(diffX / diffY);
        if (diffY < 0) {
            newRadians += Math.PI;
        } else if (diffX < 0) {
            newRadians += 2 * Math.PI;
        }

        return new Direction(Direction.radiansToDegrees(newRadians));
    }
}