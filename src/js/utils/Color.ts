export default class Color {
    r: number;
    g: number;
    b: number;
    a: number;

    constructor(r = 0, g = 0, b = 0, a = 1.0) {
        this.r = r;
        this.g = g;
        this.b = b;
        this.a = a > 1.0 ? a / 255 : a; // If opacity is greater than 1.0, than the user must be using byte notation (which is wrong)
    }

    hex(): string {
        return (this.r * Math.pow(2, 3 * 8)
            + this.g * Math.pow(2, 2 * 8)
            + this.b * Math.pow(2, 8)
            + Math.floor(this.a * 255))
            .toString(16)
            .padStart(8, "0");
    }

    rgb(): string {
        return [this.r, this.g, this.b].join(', ');
    }

    rgba(): string {
        return [this.r, this.g, this.b, this.a].join(', ');
    }
}