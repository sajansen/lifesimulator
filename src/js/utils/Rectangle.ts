import Coordinate from "./Coordinate";

export default class Rectangle {
    x: number = 0;
    y: number = 0;
    width: number = 0;
    height: number = 0;

    constructor(x = 0, y = 0, width = 0, height = 0) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
    }

    toArray(): [number, number, number, number] {
        return [this.x, this.y, this.width, this.height];
    }

    static getRectangleBetweenPoints(pointA: Coordinate, pointB: Coordinate): Rectangle {
        const rectangle = new Rectangle();
        if (pointB.x > pointA.x) {
            rectangle.x = pointA.x;
            rectangle.width = pointB.x - pointA.x;
        } else {
            rectangle.x = pointB.x;
            rectangle.width = pointA.x - pointB.x;
        }
        if (pointB.y > pointA.y) {
            rectangle.y = pointA.y;
            rectangle.height = pointB.y - pointA.y;
        } else {
            rectangle.y = pointB.y;
            rectangle.height = pointA.y - pointB.y;
        }
        return rectangle;
    }

    substractCoordinate(other: Coordinate) {
        this.x -= other.x;
        this.y -= other.y;
        return this;
    }
}