export default class Coordinate {
    x: number = null;
    y: number = null;

    constructor(x: number = 0, y: number = 0) {
        this.x = x;
        this.y = y;
    }

    toArray(): [number, number] {
        return [this.x, this.y];
    }

    distanceTo(otherCoordinate: Coordinate): number {
        return Math.sqrt(Math.pow(otherCoordinate.x - this.x, 2) + Math.pow(otherCoordinate.y - this.y, 2));
    }
}