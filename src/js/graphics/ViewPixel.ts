import Color from "../utils/Color";
import Direction from "../utils/Direction";

export default class ViewPixel {
    distance: number;
    direction: Direction;
    color: Color;

    constructor(distance: number = 0, direction: Direction = null, color: Color = null) {
        this.distance = distance;
        this.direction = direction || new Direction();
        this.color = color || new Color();
    }
}