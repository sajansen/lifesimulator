import Coordinate from "../utils/Coordinate";
import Color from "../utils/Color";
import MovingObject from "../MovingObject";
import {View} from "./View";
import './Graphics.scss';
import {GoalType} from "../Goal";

export default class Graphics {
    /** Properties of this object **/
    backgroundColor: Color = new Color(234, 255, 226);
    width: number = null;
    height: number = null;
    goalFillColor = "rgba(255,255,0,0.55)";

    objectCanvas: HTMLCanvasElement = null;
    private objectContext: CanvasRenderingContext2D = null;
    private overlayCanvas: HTMLCanvasElement = null;
    private overlayContext: CanvasRenderingContext2D = null;
    private view: View;

    setup(parentElement: HTMLElement, width: number, height: number) {
        this.width = width;
        this.height = height;

        this.view = new View(this);
        this.createCanvas(parentElement);
        this.createContext();
        this.clear();
    }

    createCanvas(parentElement: HTMLElement) {
        parentElement.style.width = `${this.width}px`;
        parentElement.style.height = `${this.height}px`;

        const objectCanvasElement = document.createElement("canvas");
        objectCanvasElement.id = "object-canvas";
        objectCanvasElement.width = this.width;
        objectCanvasElement.height = this.height;
        parentElement.appendChild(objectCanvasElement);
        this.objectCanvas = objectCanvasElement;

        const overlayCanvasElement = document.createElement("canvas");
        overlayCanvasElement.id = "overlay-canvas";
        overlayCanvasElement.width = this.width;
        overlayCanvasElement.height = this.height;
        parentElement.appendChild(overlayCanvasElement);
        this.overlayCanvas = overlayCanvasElement;
    }

    createContext() {
        this.objectContext = this.objectCanvas.getContext("2d");
        this.overlayContext = this.overlayCanvas.getContext("2d");
    }

    clear() {
        this.clearObjectCanvas();
        this.clearOverlayCanvas();
    }

    clearObjectCanvas() {
        this.objectContext.fillStyle = `rgba(${this.backgroundColor.rgba()})`;
        this.objectContext.fillRect(0, 0, this.width, this.height);
    }

    clearOverlayCanvas() {
        this.overlayContext.clearRect(0, 0, this.width, this.height);
    }

    drawMovingObject(movingObject: MovingObject) {
        if (movingObject.goal != null && !movingObject.goal.isReached
            && movingObject.goal.type === GoalType.REACH_POSITION) {
            this.drawObjectGoal(movingObject);
        }

        this.objectContext.beginPath();
        this.objectContext.fillStyle = movingObject.color;
        if (window.simulation.getFollowingObject() === movingObject)
            this.objectContext.fillStyle = "#00ff8e";
        this.objectContext.arc(movingObject.position.x, movingObject.position.y, movingObject.size, 0, 2 * Math.PI);
        this.objectContext.fill();

        if (movingObject.brakeForceApplied > 0) {
            this.drawBrakeLights(this.objectContext, movingObject);
        }

        this.drawDirectionVector(this.overlayContext, movingObject);

        if (window.simulation.getFollowingObject() === movingObject) {
            this.drawObjectInfo(this.overlayContext, movingObject);
        }
    }

    private drawBrakeLights(context: CanvasRenderingContext2D, movingObject: MovingObject) {
        context.beginPath();
        context.lineWidth = 1;
        context.strokeStyle = "#ff0000";
        context.arc(movingObject.position.x, movingObject.position.y,
            movingObject.size + 0.5,
            movingObject.direction.radians() + (-0.4 - 1.5) * Math.PI,
            movingObject.direction.radians() + (0.4 - 1.5) * Math.PI);
        context.stroke();
    }

    private drawDirectionVector(context: CanvasRenderingContext2D, movingObject: MovingObject) {
        const arrowLength = movingObject.size + 10;
        const arrowHeadPosition = new Coordinate();
        arrowHeadPosition.x = movingObject.position.x + Math.sin(movingObject.direction.radians()) * arrowLength;
        arrowHeadPosition.y = movingObject.position.y + Math.cos(movingObject.direction.radians()) * -1 * arrowLength;

        context.beginPath();
        context.strokeStyle = "#000000";
        context.lineWidth = 1;
        context.moveTo(...movingObject.position.toArray());
        context.lineTo(...arrowHeadPosition.toArray());
        context.stroke();
    }

    private drawObjectInfo(context: CanvasRenderingContext2D, movingObject: MovingObject) {
        const labelPosition = new Coordinate();
        labelPosition.x = movingObject.position.x + 20;
        labelPosition.y = movingObject.position.y + 10;

        context.beginPath();
        context.fillStyle = "#0096ff";
        context.font = "12px Arial";
        context.fillText(this.createObjectInfoText(movingObject), ...labelPosition.toArray());
    }

    createObjectInfoText(movingObject: MovingObject) {
        return `#${movingObject.id} speed: ${movingObject.speed.toFixed(1)} heading: ${movingObject.direction.degrees().toFixed(0)}°`;
    }

    drawObjectGoal(movingObject: MovingObject) {
        this.overlayContext.beginPath();
        this.overlayContext.lineWidth = 1;
        this.overlayContext.strokeStyle = "#000";
        this.overlayContext.fillStyle = this.goalFillColor;
        // @ts-ignore
        this.overlayContext.arc(...movingObject.goal.getPosition().toArray(), movingObject.size, 0, 2 * Math.PI);
        this.overlayContext.fill();
        this.overlayContext.stroke();
    }

    getImageData(x: number, y: number, width: number, height: number) {
        return this.objectContext.getImageData(x, y, width, height);
    }

    /**
     * Used for getting a View object connected to this Graphics, which can be used by any object
     */
    generateView(): View {
        return this.view;
    }

    updateObjectDetailScreen(object: MovingObject) {
        this.view.clear();
        this.view.showEyesView(object.getView());
        this.view.showTopView(object);
    }
}