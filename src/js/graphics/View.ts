import Graphics from "./Graphics";
import Coordinate from "../utils/Coordinate";
import Direction from "../utils/Direction";
import Rectangle from "../utils/Rectangle";
import ViewPixel from "./ViewPixel";
import Color from "../utils/Color";
import MovingObject from "../MovingObject";
import './View.scss';
import {GoalType} from "../Goal";

export class View {
    /** Properties of this object **/
    private eyesViewMaxPixelWidth: number = 20;
    private eyesViewPixelHeight: number = 15;
    private topViewZoomFactor: number = 2.5;
    private fieldOfViewBackgroundColor: Color = new Color(200, 200, 200);
    private viewLineColor: Color = new Color(0, 0, 0, 0.07);

    private readonly graphics: Graphics;
    private eyesViewCanvas: HTMLCanvasElement;
    private topViewCanvas: HTMLCanvasElement;
    private eyesViewContext: CanvasRenderingContext2D;
    private topViewContext: CanvasRenderingContext2D;

    constructor(graphics: Graphics) {
        this.graphics = graphics;

        const parentElement = document.getElementById('objectViewWrapper');
        this.createCanvas(parentElement);
        this.createContext();
        this.clear();
    }

    private createCanvas(parentElement: HTMLElement) {
        this.eyesViewCanvas = document.createElement("canvas");
        this.eyesViewCanvas.id = "eyes-view-canvas";
        this.eyesViewCanvas.width = this.graphics.width;
        this.eyesViewCanvas.height = this.eyesViewPixelHeight;
        parentElement.appendChild(this.eyesViewCanvas);

        this.topViewCanvas = document.createElement("canvas");
        this.topViewCanvas.id = "top-view-canvas";
        this.topViewCanvas.width = 200;
        this.topViewCanvas.height = 200;
        parentElement.appendChild(this.topViewCanvas);
    }

    private createContext() {
        this.eyesViewContext = this.eyesViewCanvas.getContext('2d');
        this.topViewContext = this.topViewCanvas.getContext('2d');
    }

    clear() {
        this.eyesViewContext.clearRect(0, 0, this.eyesViewCanvas.width, this.eyesViewCanvas.height);
        this.topViewContext.clearRect(0, 0, this.topViewCanvas.width, this.topViewCanvas.height);
    }

    /**
     * Get a array of pixels (color and distance from this position) around a position
     * @param position
     * @param direction
     * @param viewAngle
     * @param minDistance
     * @param maxDistance
     */
    getView(position: Coordinate, direction: Direction, viewAngle: number, minDistance: number, maxDistance: number): ViewPixel[] {
        if (viewAngle > 180)
            viewAngle = 180;

        if (minDistance < 0)
            minDistance = 0;

        if (maxDistance < minDistance)
            return [];

        // Get bird's eye view around our object, with the object in the center
        // The view rectangle is a rectangle around the viewable circle. This circle has
        // its center as `position` and radius as `maxDistance`.
        const viewRectangle = new Rectangle();
        viewRectangle.x = position.x - maxDistance;
        viewRectangle.y = position.y - maxDistance;
        viewRectangle.width = 2 * maxDistance + 1;
        viewRectangle.height = 2 * maxDistance + 1;

        const viewRectanglePixels = this.graphics.getImageData(...viewRectangle.toArray());
        const viewRectanglePixelsCenter = new Coordinate(viewRectangle.width / 2, viewRectangle.height / 2);

        // Sweep the view around the object for each degree in its viewAngle
        const viewPixelsPerDegree: ViewPixel[] = [];
        for (let i = direction.degrees() - viewAngle; i <= direction.degrees() + viewAngle; i++) {
            const pixelDirection = new Direction(i);

            const viewPixel = this.getFirstOccupiedPixel(
                viewRectanglePixels, viewRectanglePixelsCenter,
                pixelDirection, minDistance, maxDistance);

            // Make pixel direction relative to object's direction
            viewPixel.direction.addDegrees(-1 * direction.degrees());

            viewPixelsPerDegree.push(viewPixel);
        }

        return viewPixelsPerDegree;
    }

    /**
     * Get the first pixel in a direction which isn't a background color pixel
     * @param image
     * @param position
     * @param direction
     * @param minDistance
     * @param maxDistance
     */
    private getFirstOccupiedPixel(image: ImageData,
                                  position: Coordinate,
                                  direction: Direction,
                                  minDistance: number,
                                  maxDistance: number): ViewPixel {
        const currentPosition = new Coordinate(
            position.x + minDistance * Math.sin(direction.radians()) - 1,
            position.y - minDistance * Math.cos(direction.radians()) - 1
        );
        let currentColor: Color = null;
        while (position.distanceTo(currentPosition) < maxDistance) {
            // Get index in pixel colors array for our pixel
            // Multiply the position/index with 4, because each pixel takes 4 array elements for its color
            const imageDataIndex = 4 * (Math.round(currentPosition.y) * image.width + Math.round(currentPosition.x));
            currentColor = new Color(image.data[imageDataIndex],
                image.data[imageDataIndex + 1],
                image.data[imageDataIndex + 2],
                image.data[imageDataIndex + 3] / 255);

            if (currentColor.hex() != this.graphics.backgroundColor.hex()) {
                // Round to get the real position of the pixel we just checked. This gives better image reproducing results
                currentPosition.x = Math.round(currentPosition.x);
                currentPosition.y = Math.round(currentPosition.y);
                return new ViewPixel(position.distanceTo(currentPosition), direction, currentColor);
            }

            currentPosition.x += Math.sin(direction.radians());
            currentPosition.y -= Math.cos(direction.radians());
        }

        return new ViewPixel(-1, direction, currentColor);
    }

    /**
     * Show a bar with the 2d representation of what the object sees as blocks per ViewPixel with their color and distance
     * @param viewPixels
     */
    showEyesView(viewPixels: ViewPixel[]) {
        // Position to place this view bar
        const drawPosition = new Coordinate(0, 0);

        // Size of the view pixels
        let pixelWidth = this.eyesViewCanvas.width / viewPixels.length;
        if (pixelWidth > this.eyesViewMaxPixelWidth) {
            pixelWidth = this.eyesViewMaxPixelWidth;
            drawPosition.x = (this.eyesViewCanvas.width - pixelWidth * viewPixels.length) / 2;
        }

        // Draw view pixels
        for (let i = 0; i < viewPixels.length; i++) {
            this.eyesViewContext.beginPath();
            this.eyesViewContext.fillStyle = `#${viewPixels[i].color.hex()}`;
            this.eyesViewContext.fillRect(drawPosition.x + i * pixelWidth, drawPosition.y, pixelWidth, this.eyesViewPixelHeight);

            if (viewPixels[i].distance < 0)
                continue;

            // Display distance of pixel
            this.eyesViewContext.font = "9.5px Arial";
            this.eyesViewContext.fillStyle = "#000";
            if (viewPixels[i].color.r < 100 && viewPixels[i].color.g < 100 && viewPixels[i].color.b < 100 && viewPixels[i].color.a > 0.6) {
                this.eyesViewContext.fillStyle = "#fff";
            }
            this.eyesViewContext.fillText(Math.round(viewPixels[i].distance).toString(), drawPosition.x + i * pixelWidth + 2, drawPosition.y + 12);
        }
    }

    /**
     * Show a top view of the field of view of the object, with the ViewPixels at their position relative to the object
     * @param direction
     * @param viewAngle
     * @param minDistance
     * @param maxDistance
     * @param viewPixels
     */
    showTopView(object: MovingObject) {
        // Position inside the canvas to draw this image
        const imagePosition = new Coordinate(0, 2);
        const center = new Coordinate(imagePosition.x + object.maxViewDistance, imagePosition.y + object.maxViewDistance);

        this.topViewCanvas.width = 2 * object.maxViewDistance * this.topViewZoomFactor;
        this.topViewCanvas.height = object.maxViewDistance
            + Math.max(0, object.maxViewDistance * -1 * Math.cos(Direction.degreesToRadian(object.viewAngle)))
            + imagePosition.y
            + 2;
        this.topViewCanvas.height = this.topViewCanvas.height * this.topViewZoomFactor;
        this.topViewContext.scale(this.topViewZoomFactor, this.topViewZoomFactor);

        this.drawFieldOfView(this.topViewContext,
            center, new Direction(0),
            object.viewAngle, object.minViewDistance, object.maxViewDistance,
            true, this.fieldOfViewBackgroundColor);

        // Draw minimum braking distance line
        this.topViewContext.beginPath();
        this.topViewContext.lineWidth = 1;
        this.topViewContext.strokeStyle = "rgba(167,0,7,0.25)";
        this.topViewContext.arc(center.x, center.y, object.getMinimalBrakeDistance(),
            Direction.degreesToRadian(-1 * object.viewAngle) - 0.5 * Math.PI,
            Direction.degreesToRadian(object.viewAngle) - 0.5 * Math.PI);
        this.topViewContext.stroke();

        // Draw view pixels
        object.getView().forEach(pixel => {
            if (pixel.distance < 0) {
                const viewStopPosition = new Coordinate(center.x, center.y);

                viewStopPosition.x += object.maxViewDistance * Math.sin(pixel.direction.radians());
                viewStopPosition.y -= object.maxViewDistance * Math.cos(pixel.direction.radians());
                this.viewLineTo(this.topViewContext, center, viewStopPosition);
                return;
            }

            const pixelPosition = new Coordinate(center.x, center.y);
            pixelPosition.x += pixel.distance * Math.sin(pixel.direction.radians());
            pixelPosition.y -= pixel.distance * Math.cos(pixel.direction.radians());

            this.viewLineTo(this.topViewContext, center, pixelPosition);

            this.topViewContext.beginPath();
            this.topViewContext.fillStyle = "#000";
            this.topViewContext.fillRect(pixelPosition.x, pixelPosition.y, 1, 1);
        });

        if (object.goal != null && !object.goal.isReached
            && [GoalType.REACH_POSITION, GoalType.FOLLOW_CURSOR].includes(object.goal.type)) {
            this.drawGoalArrowOnTopView(object, center);
        }
    }

    private viewLineTo(context: CanvasRenderingContext2D, startPosition: Coordinate, stopPosition: Coordinate) {
        context.beginPath();
        context.lineWidth = 1;
        context.strokeStyle = `rgba(${this.viewLineColor.rgba()})`;
        context.moveTo(...startPosition.toArray());
        context.lineTo(...stopPosition.toArray());
        context.stroke();
    }

    private drawGoalArrowOnTopView(object: MovingObject, center) {
        const goalDirection = new Direction(object.getDirectionToGoal().degrees() - object.previousDirection.degrees(), false);

        const goalPosition = new Coordinate(
            center.x + (object.maxViewDistance + 2) * Math.sin(goalDirection.radians()),
            center.y + (object.maxViewDistance + 2) * -1 * Math.cos(goalDirection.radians())
        );
        this.topViewContext.beginPath();
        this.topViewContext.moveTo(...goalPosition.toArray());
        this.topViewContext.arc(goalPosition.x, goalPosition.y, 4,
            goalDirection.radians() - 0.5 - 1.5 * Math.PI,
            goalDirection.radians() + 0.5 - 1.5 * Math.PI,
        );
        this.topViewContext.fillStyle = this.graphics.goalFillColor;
        this.topViewContext.fill();
        this.topViewContext.strokeStyle = "rgba(0,0,0,0.39)";
        this.topViewContext.stroke();
    }

    private drawFieldOfView(context: CanvasRenderingContext2D, centerPosition: Coordinate,
                            direction: Direction, viewAngle: number,
                            minDistance: number, maxDistance: number,
                            drawBackground: boolean = true, backgroundColor: Color = null) {
        const leftFlankDirection = new Direction(direction.degrees() - viewAngle, false);
        const rightFlankDirection = new Direction(direction.degrees() + viewAngle, false);

        // Draw view border lines
        const leftFlankPositionStart = new Coordinate(
            centerPosition.x + minDistance * Math.sin(leftFlankDirection.radians()),
            centerPosition.y - minDistance * Math.cos(leftFlankDirection.radians()));
        const leftFlankPositionStop = new Coordinate(
            centerPosition.x + maxDistance * Math.sin(leftFlankDirection.radians()),
            centerPosition.y - maxDistance * Math.cos(leftFlankDirection.radians()));

        const rightFlankPositionStart = new Coordinate(
            centerPosition.x + minDistance * Math.sin(rightFlankDirection.radians()),
            centerPosition.y - minDistance * Math.cos(rightFlankDirection.radians()));
        const rightFlankPositionStop = new Coordinate(
            centerPosition.x + maxDistance * Math.sin(rightFlankDirection.radians()),
            centerPosition.y - maxDistance * Math.cos(rightFlankDirection.radians()));

        if (drawBackground && backgroundColor != null) {
            // Draw background
            context.beginPath();
            context.moveTo(...centerPosition.toArray());
            context.fillStyle = `rgba(${backgroundColor.rgba()})`

        ;
                    context.arc(centerPosition.x, centerPosition.y, maxDistance,
                        leftFlankDirection.radians() - 0.5 * Math.PI,
                        rightFlankDirection.radians() - 0.5 * Math.PI);
                    context.fill();
                }

                context.strokeStyle = "#a2a2a2";
                context.lineWidth = 1;
                context.beginPath();
                context.moveTo(...leftFlankPositionStart.toArray());
                context.lineTo(...leftFlankPositionStop.toArray());
                context.stroke();

                context.beginPath();
                context.moveTo(...rightFlankPositionStart.toArray());
                context.lineTo(...rightFlankPositionStop.toArray());
                context.stroke();

                // Draw inner view radius
                context.beginPath();
                context.arc(centerPosition.x, centerPosition.y, minDistance,
                    leftFlankDirection.radians() - 0.5 * Math.PI,
                    rightFlankDirection.radians() - 0.5 * Math.PI);
                context.stroke();

                // Draw outer view radius
                context.beginPath();
                context.arc(centerPosition.x, centerPosition.y, maxDistance,
                    leftFlankDirection.radians() - 0.5 * Math.PI,
                    rightFlankDirection.radians() - 0.5 * Math.PI);
                context.stroke();
            }
        }
