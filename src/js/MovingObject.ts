import Graphics from "./graphics/Graphics";
import Coordinate from "./utils/Coordinate";
import Direction from "./utils/Direction";
import {View} from "./graphics/View";
import ViewPixel from "./graphics/ViewPixel";
import Goal, {GoalType} from "./Goal";

export default class MovingObject {

    /** The properties of this object **/
    /* Appearance */
    // The color of the object
    color: string = null;
    // The size of the object (radius)
    size: number = 4;

    /* Physics */
    // The mass of the object (kg)
    mass: number = 50;
    // The maxAccelerateForce the object can produce (N)
    maxAccelerateForce: number = 2;
    maxBrakeForce: number = 4;

    /* Movement */
    // The maximum speed the object may have
    maxSpeed: number = 5;
    // Maximum rotation (degrees) the object can do, like in a turn
    maxRotation: number = 5;
    frictionCoefficient: number = 0.005;

    /* Sight */
    // The angle to both sides which the object can see
    viewAngle: number = 40;
    // The maximum distance/radius the object can see
    maxViewDistance: number = 140;
    // The minimum distance/radius the object can see from beyond his own body (`size`)
    minViewDistance: number = this.size + 3;


    /** The state of this object **/
    readonly id: number;
    speed: number = 0;
    position: Coordinate = new Coordinate();
    direction: Direction = new Direction();
    goal: Goal = null;
    waitTimer: number = null;
    // The graphics object where this object is being displayed / living
    readonly graphics: Graphics;
    private viewPixels: ViewPixel[] = [];

    private readonly view: View;
    previousDirection: Direction = new Direction();
    brakeForceApplied: number = 0;

    constructor(id: number, graphics = null) {
        this.id = id;
        this.graphics = graphics;

        if (this.graphics != null)
            this.view = this.graphics.generateView();
    }

    step() {
        this.previousDirection = this.direction;

        this.see();

        if (this.goal == null || this.goal.isReached || this.wait()) {
            // Nothing to do. Wait until a new meaning to life is given
            this.brake(this.maxBrakeForce);
            return;
        }


        if (this.isObjectInFront()) {
            this.brake(this.maxBrakeForce);
            return;
        }

        if (this.goal.type === GoalType.REACH_POSITION) {
            this.stepGoalReachPosition();
            return;
        }
        if (this.goal.type === GoalType.FOLLOW_CURSOR) {
            this.stepGoalFollowCursor();
            return;
        }

        this.accelerate(0);
    }

    stepGoalReachPosition() {
        if (this.isGoalReached()) {
            this.brake(this.maxBrakeForce);
            this.goal.isReached = true;
            this.waitTimer = Math.random() * 50 + 20;
            return;
        }

        let rotationDegrees = this.getRotationDegreesToGoal();
        rotationDegrees = this.correctRotationDegreesForTurnRadius(rotationDegrees);
        this.rotate(rotationDegrees);

        if (this.getDistanceToGoal() <= this.getMinimalBrakeDistance()) {
            this.brake(this.maxBrakeForce);
            return;
        }

        this.accelerate(this.maxAccelerateForce);
    }

    stepGoalFollowCursor() {
        if (this.isGoalReached()) {
            this.brake(this.maxBrakeForce);
            return;
        }

        let rotationDegrees = this.getRotationDegreesToGoal();
        rotationDegrees = this.correctRotationDegreesForTurnRadius(rotationDegrees);
        this.rotate(rotationDegrees);

        if (this.getDistanceToGoal() <= this.getMinimalBrakeDistance()) {
            this.brake(this.maxBrakeForce);
            return;
        }

        this.accelerate(this.maxAccelerateForce);
    }

    stop() {
        this.speed = 0;
    }

    brake(force: number) {
        this.brakeForceApplied = force / this.mass;
        this.move(0);
    }

    accelerate(force: number) {
        this.brakeForceApplied = 0;
        this.move(force / this.mass);
    }

    move(acceleration: number) {
        if (this.direction.degrees() != this.previousDirection.degrees()) {
            acceleration *= 0.9;    // Take off 10% of the acceleration
        }

        this.speed += acceleration;

        const frictionApplied = this.brakeForceApplied + this.frictionCoefficient;
        if (this.speed > frictionApplied) {
            this.speed -= frictionApplied;
        } else if (this.speed < -1 * frictionApplied) {
            this.speed += frictionApplied;
        } else {
            this.speed = 0;
        }


        const newPosition = new Coordinate();

        newPosition.x = this.position.x + this.speed * Math.sin(this.direction.radians());
        newPosition.y = this.position.y + this.speed * -1 * Math.cos(this.direction.radians());

        this.correctForCanvasBoundaries(newPosition);

        this.position = newPosition;
    }

    getMinimalBrakeDistance() {
        return this.mass * Math.pow(this.speed, 2) / (2 * this.maxBrakeForce);
    }

    correctForCanvasBoundaries(proposedPosition: Coordinate) {
        if (this.graphics == null) {
            return;
        }

        if (proposedPosition.x < 0) {
            proposedPosition.x = 0;
            this.flipOverX();
        } else if (proposedPosition.x > this.graphics.width) {
            proposedPosition.x = this.graphics.width;
            this.flipOverX();
        }

        if (proposedPosition.y < 0) {
            proposedPosition.y = 0;
            this.flipOverY();
        } else if (proposedPosition.y > this.graphics.height) {
            proposedPosition.y = this.graphics.height;
            this.flipOverY();
        }
    }

    rotate(degrees: number) {
        this.direction.addDegrees(degrees);
    }

    flipOverY() {
        if (this.direction.degrees() <= 180)
            this.direction.setDegrees(180 - this.direction.degrees());
        else
            this.direction.setDegrees(360 - (this.direction.degrees() - 180));
    }

    flipOverX() {
        this.direction.setDegrees(360 - this.direction.degrees());
    }

    setGoal(goal: Goal) {
        this.goal = goal;
    }

    getDistanceToGoal(): number {
        return this.position.distanceTo(this.goal.getPosition());
    }

    getRotationDegreesToGoal(): number {
        let rotationDegrees = this.getDirectionToGoal().degrees() - this.direction.degrees();

        if (rotationDegrees > 180) {
            rotationDegrees -= 360;
        } else if (rotationDegrees < -180) {
            rotationDegrees += 360;
        }

        return rotationDegrees;
    }

    correctRotationDegreesForTurnRadius(rotationDegrees: number): number {
        if (Math.abs(rotationDegrees) <= this.maxRotation) {
            return rotationDegrees;
        }

        rotationDegrees = this.maxRotation * (rotationDegrees > 0 ? 1 : -1);

        // Check if we can make the turn
        if (!this.isTargetPositionInsideTurnRadius(this.goal.getPosition())) {
            return rotationDegrees;
        }

        return -1 * this.maxRotation;     // rotate to the other side
    }

    getDirectionToGoal(): Direction {
        return Direction.getDirectionTo(this.position, this.goal.getPosition())
    }

    isGoalReached(): boolean {
        return this.position.distanceTo(this.goal.getPosition()) <= this.goal.margin;
    }

    wait(): boolean {
        if (this.waitTimer == null)
            return false;

        this.waitTimer--;

        if (this.waitTimer <= 0) {
            this.waitTimer = null;
            return false;
        }
        return true;
    }

    isTargetPositionInsideTurnRadius(targetPosition: Coordinate): boolean {
        const turnRadius = this.speed / Math.tan(2 * Direction.degreesToRadian(this.maxRotation));

        // Quick check before we do complicated stuff
        if (this.position.distanceTo(targetPosition) > 2 * turnRadius)
            return false;

        const targetDirection = Direction.getDirectionTo(this.position, targetPosition);
        const turnCenterDirection = new Direction(this.direction.degrees() + (targetDirection.isLeftFrom(this.direction) ? -90 : 90));
        const turnCenterPosition = new Coordinate(
            this.position.x + turnRadius * Math.sin(turnCenterDirection.radians()),
            this.position.y - turnRadius * Math.cos(turnCenterDirection.radians()));

        return turnCenterPosition.distanceTo(targetPosition) <= turnRadius - 5;
    }

    see() {
        this.viewPixels = this.view.getView(this.position, this.direction,
            this.viewAngle, this.minViewDistance, this.maxViewDistance);
    }

    getView(): ViewPixel[] {
        return this.viewPixels;
    }

    private isObjectInFront(): boolean {
        const minimalBrakeDistance = this.getMinimalBrakeDistance();
        const sortedViewPixels = this.viewPixels.filter(pixel => pixel.distance >= 0 && pixel.distance <= minimalBrakeDistance);

        if (sortedViewPixels.length === 0) {
            return false;
        }

        sortedViewPixels.sort((pixelA, pixelB) => pixelA.distance - pixelB.distance);

        // Slow down when it's the field's border
        if (sortedViewPixels[0].color.a === 0) {
            return true;
        }

        if (this.direction.isLeftFrom(sortedViewPixels[0].direction)) {
            this.rotate(-1 * this.maxRotation);
        } else {
            this.rotate(this.maxRotation);
        }
        return true;
    }
}