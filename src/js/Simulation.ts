import Coordinate from "./utils/Coordinate";
import Graphics from "./graphics/Graphics";
import MovingObject from "./MovingObject";
import Goal, {GoalType} from "./Goal";

export default class Simulation {
    private static stepsPerSecond: number = 25;
    private static isStepping: boolean = false;
    private static currentStep: number = 0;
    private fpsCounterIntervalHook: number = null;
    private stepIntervalHook: number = null;
    private fpsCounter: number = 0;
    private fpsCounterInterval: number = 500; // in milliseconds
    graphics: Graphics = new Graphics();
    objects: MovingObject[] = [];
    private followingObject: MovingObject | null = null;
    private isPaused: boolean = true;
    private animationFrameRequest: number = -1;

    constructor() {
        // For debug purposes only
        window.simulation = window.simulation || this;

        this.showAndResetFPS = this.showAndResetFPS.bind(this);
        this.step = this.step.bind(this);
        this.updateGraphics = this.updateGraphics.bind(this);
    }

    setup(canvasParentElement) {
        this.graphics.setup(canvasParentElement, 800, 500)
    }

    start() {
        this.clearIntervals();
        this.fpsCounterIntervalHook = window.setInterval(this.showAndResetFPS, this.fpsCounterInterval);

        if (Simulation.stepsPerSecond > 0)
            this.stepIntervalHook = window.setInterval(this.step, 1000 / Simulation.stepsPerSecond);

        this.isPaused = false;
    }

    pause() {
        this.clearIntervals();
        this.isPaused = true;
    }

    private clearIntervals() {
        window.clearInterval(this.fpsCounterIntervalHook);
        window.clearInterval(this.stepIntervalHook);
    }

    setSimulationSpeed(stepsPerSecond: number) {
        Simulation.stepsPerSecond = stepsPerSecond;
        this.pause();
        this.start();
    }

    static getStepsPerSecond(): number {
        return Simulation.stepsPerSecond;
    }

    private checkIfSimulationIsLagging(stepStartTime: number) {
        const now = (new Date()).getTime();
        const maxAllowedDifference = 1000 / Simulation.stepsPerSecond;   // Expect to be one, not two or more, steps further
        const laggTime = now - stepStartTime - maxAllowedDifference;
        if (laggTime > 0) {
            console.warn(`Simulation is lagging: ${Math.round(laggTime)} ms`);
        }
    }

    step() {
        if (Simulation.isStepping) {
            console.warn("Skipping step because previous step hasn't completed yet");
            return;
        }

        Simulation.isStepping = true;
        const stepStartTime = (new Date()).getTime();
        Simulation.currentStep++;

        this.updateObjects();

        if (this.hasObjectCollision()) {
            // this.pause();
            console.info("Collision detected!");
        }

        cancelAnimationFrame(this.animationFrameRequest);   // Cancel any pending animation frame requests, because we're going to update it anyway
        this.animationFrameRequest = requestAnimationFrame(this.updateGraphics);

        Simulation.isStepping = false;
        this.checkIfSimulationIsLagging(stepStartTime);
    }

    showAndResetFPS() {
        document.getElementById("fps").innerText =
            Math.round(this.fpsCounter * 1000 / this.fpsCounterInterval).toString();
        this.fpsCounter = 0;
    }

    updateObjects() {
        this.objects.forEach((object: MovingObject) => {
            // const rotation = (Math.random() - 0.5) * 30;
            // object.rotate(rotation);

            if (object.goal == null || object.goal.isReached) {
                this.setGoalForObject(object);
            }

            object.step();
        });
    }

    private updateGraphics() {
        this.graphics.clear();
        this.objects.forEach((object: MovingObject) => {
            this.graphics.drawMovingObject(object);
        });

        if (this.followingObject != null) {
            this.graphics.updateObjectDetailScreen(this.followingObject);
        }
        this.fpsCounter++;
    }

    setGoalForObject(object: MovingObject) {
        const randomGoalCoordinate = new Coordinate();
        randomGoalCoordinate.x = Math.random() * this.graphics.width;
        randomGoalCoordinate.y = Math.random() * this.graphics.height;

        const goal = new Goal(GoalType.REACH_POSITION);
        goal.setPosition(randomGoalCoordinate);

        object.setGoal(goal);
    }

    setFollowingObject(object: MovingObject) {
        this.followingObject = object;

        if (!this.isPaused)
            return;

        this.updateGraphics();
    }

    getFollowingObject(): MovingObject {
        return this.followingObject;
    }

    hasObjectCollision(): boolean {
        let hasCollision: boolean = false;
        this.objects.sort((objectA, objectB) => {
            if (hasCollision) {
                return -1;  // Quick way to end this function
            }

            if (objectA.position.distanceTo(objectB.position) <= objectA.size + objectB.size) {
                hasCollision = true;
                return -1;
            }

            // To check if pixels from objectB slightly to the left of objectA might collide
            // with another pixel at the same x as objectA
            return objectA.position.x - objectB.position.x + 2 * objectB.size;
        });
        return hasCollision;
    }

    static getCurrentStep(): number {
        return Simulation.currentStep;
    }
}