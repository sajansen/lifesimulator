import Coordinate from "./utils/Coordinate";

export default class Goal {
    type: GoalType;
    isReached: boolean = false;
    private position: Coordinate = null;

    // The radius around the goal in which the object must be to mark the goal as reached
    margin: number = 5;

    constructor(type: GoalType) {
        this.type = type;
    }

    getPosition(): Coordinate {
        if (this.type === GoalType.REACH_POSITION) {
            return this.position;
        }
        if (this.type === GoalType.FOLLOW_CURSOR) {
            return window.cursorPosition;
        }
        return null;
    }

    setPosition(position: Coordinate) {
        this.position = position;
    }
}

export enum GoalType {
    REACH_POSITION,
    FOLLOW_CURSOR
}