import './style.scss';
import Simulation from "./js/Simulation";
import MovingObject from "./js/MovingObject";
import Coordinate from "./js/utils/Coordinate";
import Goal, {GoalType} from "./js/Goal";


declare global {
    interface Window {
        simulation: any
        cursorPosition: Coordinate
    }
}

const canvasParentElement = document.getElementById("canvas");
const simulation = new Simulation();
simulation.setup(canvasParentElement);

for (let i = 0; i < 10; i++) {
    const object = new MovingObject(i + 1, simulation.graphics);
    object.color = "#0000ff";
    object.mass = Math.random() * 80 + 20;
    object.position.x = Math.random() * simulation.graphics.width;
    object.position.y = Math.random() * simulation.graphics.height;
    object.direction.setRadians(Math.random() * 2 * Math.PI);
    simulation.objects.push(object);
}

const object = new MovingObject(1, simulation.graphics);
object.color = "#0000ff";
object.position.x = simulation.graphics.width / 2;
object.position.y = simulation.graphics.height / 2;
object.direction.setRadians(0.5 * Math.PI);
const goal = new Goal(GoalType.FOLLOW_CURSOR);
goal.setPosition(new Coordinate(object.position.x + 50, object.position.y));
object.setGoal(goal);

// simulation.objects.push(object);

simulation.setSimulationSpeed(16);
simulation.setFollowingObject(simulation.objects[0]);
simulation.start();



function setFollowingObjectToId(id: number) {
    const object = simulation.objects.find(object => object.id === id);
    simulation.setFollowingObject(object);

    document.getElementById("objectViewWrapper").style.display = "block";
    if (object != null)
        return;

    document.getElementById("objectViewWrapper").style.display = "none";
}

function followingObjectSelectChange() {
    setFollowingObjectToId(parseInt(this.value));
}

const followingObjectSelectElement = document.getElementById("followingObjectSelectElement");
followingObjectSelectElement.addEventListener("change", followingObjectSelectChange);
simulation.objects.forEach(object => {
    const optionElement = document.createElement('option');
    optionElement.value = object.id.toString();
    optionElement.text = object.id.toString();
    if (simulation.getFollowingObject() == object)
        optionElement.selected = true;
    followingObjectSelectElement.appendChild(optionElement);
});


window.cursorPosition = new Coordinate();
function getCursorPosition(e: MouseEvent) {
    window.cursorPosition.x = e.clientX - simulation.graphics.objectCanvas.parentElement.offsetLeft;
    window.cursorPosition.y = e.clientY - simulation.graphics.objectCanvas.parentElement.offsetTop;
}
document.addEventListener('mousemove', getCursorPosition);