import Color from "../src/js/utils/Color";

const assert = require('assert');

describe("Color", function () {
    it("should give the right hex value", function () {
        assert.equal(new Color(0, 0, 0, 0.0).hex(), "00000000");
        assert.equal(new Color(0, 0, 0, 0.5).hex(), "0000007f");
        assert.equal(new Color(0, 0, 0, 1.0).hex(), "000000ff");
        assert.equal(new Color(255, 255, 255, 0.0).hex(), "ffffff00");
        assert.equal(new Color(0, 255, 0, 0.5).hex(), "00ff007f");
        assert.equal(new Color(255, 255, 255, 1.0).hex(), "ffffffff");
    });
});