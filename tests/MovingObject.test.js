import MovingObject from "~/js/MovingObject";
import Coordinate from "~/js/utils/Coordinate";
import Goal, {GoalType} from "~/js/Goal";

const assert = require('assert');

describe("MovingObject", function () {
  const graphics = {width: 500, height: 500};
  graphics.generateView = () => null;
  const movingObject = new MovingObject(1, graphics);

  describe("flipOverX", function () {
    it("should return 270 when initial direction is 90", function () {
      movingObject.direction.setDegrees(90);
      movingObject.flipOverX();
      const actualDegrees = Math.round(movingObject.direction.degrees());
      assert.equal(actualDegrees, 270);
    });
    it("should return 180 when initial direction is 180", function () {
      movingObject.direction.setDegrees(180);
      movingObject.flipOverX();
      const actualDegrees = Math.round(movingObject.direction.degrees());
      assert.equal(actualDegrees, 180);
    });
    it("should return 160 when initial direction is 200", function () {
      movingObject.direction.setDegrees(200);
      movingObject.flipOverX();
      const actualDegrees = Math.round(movingObject.direction.degrees());
      assert.equal(actualDegrees, 160);
    });
    it("should return 359 when initial direction is 1", function () {
      movingObject.direction.setDegrees(1);
      movingObject.flipOverX();
      const actualDegrees = Math.round(movingObject.direction.degrees());
      assert.equal(actualDegrees, 359);
    });
  });

  describe("flipOverY", function () {
    it("should return 180 when initial direction is 0", function () {
      movingObject.direction.setDegrees(0);
      movingObject.flipOverY();
      const actualDegrees = Math.round(movingObject.direction.degrees());
      assert.equal(actualDegrees, 180);
    });
    it("should return 90 when initial direction is 90", function () {
      movingObject.direction.setDegrees(90);
      movingObject.flipOverY();
      const actualDegrees = Math.round(movingObject.direction.degrees());
      assert.equal(actualDegrees, 90);
    });
    it("should return 135 when initial direction is 45", function () {
      movingObject.direction.setDegrees(45);
      movingObject.flipOverY();
      const actualDegrees = Math.round(movingObject.direction.degrees());
      assert.equal(actualDegrees, 135);
    });
    it("should return 45 when initial direction is 135", function () {
      movingObject.direction.setDegrees(135);
      movingObject.flipOverY();
      const actualDegrees = Math.round(movingObject.direction.degrees());
      assert.equal(actualDegrees, 45);
    });
    it("should return 350 when initial direction is 190", function () {
      movingObject.direction.setDegrees(190);
      movingObject.flipOverY();
      const actualDegrees = Math.round(movingObject.direction.degrees());
      assert.equal(actualDegrees, 350);
    });
  });

  describe("rotate", function () {
    it("should let the object rotate to 80 if the initial direction is 60 and rotate() is called with value 20 degrees", function () {
      movingObject.direction.setDegrees(60);
      movingObject.rotate(20);
      const actualDegrees = Math.round(movingObject.direction.degrees());
      assert.equal(actualDegrees, 80);
    });
    it("should let the object rotate to 40 if the initial direction is 60 and rotate() is called with value -20 degrees", function () {
      movingObject.direction.setDegrees(60);
      movingObject.rotate(-20);
      const actualDegrees = Math.round(movingObject.direction.degrees());
      assert.equal(actualDegrees, 40);
    });
  });

  describe("move", function () {
    movingObject.maxSpeed = 10;

    it("should move horizontal forwards when direction is 90", function () {
      movingObject.position = new Coordinate(10, 10);
      movingObject.direction.setDegrees(90);
      movingObject.speed = 10;
      movingObject.move(0);

      const actualPositionX = Math.round(movingObject.position.x);
      const actualPositionY = Math.round(movingObject.position.y);
      assert.equal(actualPositionX, 20);
      assert.equal(actualPositionY, 10);
    });

    it("should move vertical downward when direction is 180", function () {
      movingObject.position = new Coordinate(10, 10);
      movingObject.direction.setDegrees(180);
      movingObject.speed = 10;
      movingObject.move(0);

      const actualPositionX = Math.round(movingObject.position.x);
      const actualPositionY = Math.round(movingObject.position.y);
      assert.equal(actualPositionX, 10);
      assert.equal(actualPositionY, 20);
    });

    it("should move diagonally backwards and upwards when direction is 315", function () {
      movingObject.position = new Coordinate(20, 20);
      movingObject.direction.setDegrees(315);
      movingObject.speed = 10;
      movingObject.move(0);

      const actualPositionX = Math.round(movingObject.position.x);
      const actualPositionY = Math.round(movingObject.position.y);
      assert.equal(actualPositionX, 13);
      assert.equal(actualPositionY, 13);
    });
  });

  describe("correctForCanvasBoundaries", function () {
    it("should place the object within objectCanvas boundaries when it's outside the boundaries (x < 0)", function () {
      movingObject.position = new Coordinate(-1, 0);
      movingObject.direction.setDegrees(270);
      movingObject.correctForCanvasBoundaries(movingObject.position);

      const actualPositionX = Math.round(movingObject.position.x);
      const actualPositionY = Math.round(movingObject.position.y);
      const actualDegrees = Math.round(movingObject.direction.degrees());
      assert.equal(actualPositionX, 0);
      assert.equal(actualPositionY, 0);
      assert.equal(actualDegrees, 90);
    });
    it("should place the object within objectCanvas boundaries when it's outside the boundaries (x > width)", function () {
      const maxWidth = graphics.width;
      movingObject.position = new Coordinate(maxWidth + 1, 0);
      movingObject.direction.setDegrees(10);
      movingObject.correctForCanvasBoundaries(movingObject.position);

      const actualPositionX = Math.round(movingObject.position.x);
      const actualPositionY = Math.round(movingObject.position.y);
      const actualDegrees = Math.round(movingObject.direction.degrees());
      assert.equal(actualPositionX, maxWidth);
      assert.equal(actualPositionY, 0);
      assert.equal(actualDegrees, 350);
    });
    it("should place the object within objectCanvas boundaries when it's outside the boundaries (x < 0 and y < 0)", function () {
      movingObject.position = new Coordinate(-100, -1);
      movingObject.direction.setDegrees(350);
      movingObject.correctForCanvasBoundaries(movingObject.position);

      const actualPositionX = Math.round(movingObject.position.x);
      const actualPositionY = Math.round(movingObject.position.y);
      const actualDegrees = Math.round(movingObject.direction.degrees());
      assert.equal(actualPositionX, 0);
      assert.equal(actualPositionY, 0);
      assert.equal(actualDegrees, 170);
    });
  });

  describe("getDirectionOfGoalFromMe", function () {
    beforeEach(function () {
      movingObject.maxRotation = 30;
      movingObject.position = new Coordinate(20, 20);
      movingObject.direction.setDegrees(90);
    });

    it("should give the right direction when goal is in front of the object", function () {
      const goal = new Goal(GoalType.REACH_POSITION);
      goal.position = new Coordinate(50, 20);
      movingObject.setGoal(goal);

      const actualDirection = Math.round(movingObject.getDirectionToGoal().degrees());
      assert.equal(actualDirection, 90);
    });

    it("should give the right direction when goal is slightly to the right of the object", function () {
      const goal = new Goal(GoalType.REACH_POSITION);
      goal.position = new Coordinate(50, 30);
      movingObject.setGoal(goal);

      const actualDirection = Math.round(movingObject.getDirectionToGoal().degrees());
      assert.equal(actualDirection, 108);
    });
    it("should give the right direction when goal is all the way to the right of the object", function () {
      const goal = new Goal(GoalType.REACH_POSITION);
      goal.position = new Coordinate(20, 50);
      movingObject.setGoal(goal);

      const actualDirection = Math.round(movingObject.getDirectionToGoal().degrees());
      assert.equal(actualDirection, 180);
    });
    it("should give the right direction when goal is at the back right of the object", function () {
      const goal = new Goal(GoalType.REACH_POSITION);
      goal.position = new Coordinate(0, 30);
      movingObject.setGoal(goal);

      const actualDirection = Math.round(movingObject.getDirectionToGoal().degrees());
      assert.equal(actualDirection, 243);
    });

    it("should give the right direction when goal is slightly to the left of the object", function () {
      const goal = new Goal(GoalType.REACH_POSITION);
      goal.position = new Coordinate(50, 10);
      movingObject.setGoal(goal);

      const actualDirection = Math.round(movingObject.getDirectionToGoal().degrees());
      assert.equal(actualDirection, 72);
    });
    it("should give the right direction when goal is all the way to the left of the object", function () {
      const goal = new Goal(GoalType.REACH_POSITION);
      goal.position = new Coordinate(20, 0);
      movingObject.setGoal(goal);

      const actualDirection = Math.round(movingObject.getDirectionToGoal().degrees());
      assert.equal(actualDirection, 0);
    });
    it("should give the right direction when goal is at the back left of the object", function () {
      const goal = new Goal(GoalType.REACH_POSITION);
      goal.position = new Coordinate(0, 10);
      movingObject.setGoal(goal);

      const actualDirection = Math.round(movingObject.getDirectionToGoal().degrees());
      assert.equal(actualDirection, 297);
    });


    it("should give the right direction when goal is at the back right, far from the object", function () {
      movingObject.position = new Coordinate(200, 200);
      movingObject.direction.setDegrees(0);
      const goal = new Goal(GoalType.REACH_POSITION);
      goal.position = new Coordinate(100, 300);
      movingObject.setGoal(goal);

      const actualDirection = Math.round(movingObject.getDirectionToGoal().degrees());
      assert.equal(actualDirection, 225);
    });
  });

  describe("isGoalReached", function () {
    beforeEach(function () {
      movingObject.goal.margin = 5;
    });

    it("should be true when the object is within 5 pixels from the goal in the x direction", function () {
      const goal = new Goal(GoalType.REACH_POSITION);
      goal.position = new Coordinate(20, 20);
      movingObject.setGoal(goal);
      movingObject.position = new Coordinate(15, 20);
      assert.equal(movingObject.isGoalReached(), true);

      movingObject.position = new Coordinate(25, 20);
      assert.equal(movingObject.isGoalReached(), true);
    });
    it("should be false when the object is more than 5 pixels from the goal in the x direction", function () {
      const goal = new Goal(GoalType.REACH_POSITION);
      goal.position = new Coordinate(20, 20);
      movingObject.setGoal(goal);
      movingObject.position = new Coordinate(14, 20);
      assert.equal(movingObject.isGoalReached(), false);

      movingObject.position = new Coordinate(30, 20);
      assert.equal(movingObject.isGoalReached(), false);
    });

    it("should be true when the object is within 5 pixels from the goal in the y direction", function () {
      const goal = new Goal(GoalType.REACH_POSITION);
      goal.position = new Coordinate(20, 20);
      movingObject.setGoal(goal);
      movingObject.position = new Coordinate(20, 15);
      assert.equal(movingObject.isGoalReached(), true);

      movingObject.position = new Coordinate(20, 25);
      assert.equal(movingObject.isGoalReached(), true);
    });
    it("should be false when the object is more than 5 pixels from the goal in the y direction", function () {
      const goal = new Goal(GoalType.REACH_POSITION);
      goal.position = new Coordinate(20, 20);
      movingObject.setGoal(goal);
      movingObject.position = new Coordinate(20, 14);
      assert.equal(movingObject.isGoalReached(), false);

      movingObject.position = new Coordinate(20, 30);
      assert.equal(movingObject.isGoalReached(), false);
    });

    it("should be false when the object is more than 5 pixels from the goal in the any direction", function () {
      const goal = new Goal(GoalType.REACH_POSITION);
      goal.position = new Coordinate(20, 20);
      movingObject.setGoal(goal);
      movingObject.position = new Coordinate(10, 10);
      assert.equal(movingObject.isGoalReached(), false);

      movingObject.position = new Coordinate(30, 30);
      assert.equal(movingObject.isGoalReached(), false);
    });
  });

  describe("getDistanceToGoal", function () {
    beforeEach(function () {
      movingObject.position = new Coordinate(100, 100);
    });

    it("should return the right distance when the goal is far away from me", function () {
      movingObject.goal = new Goal(GoalType.REACH_POSITION);
      movingObject.goal.position = new Coordinate(500, 500);

      const actualDistance = Math.round(movingObject.getDistanceToGoal());
      assert.equal(actualDistance, 566);
    });
    it("should return the right distance when the goal is close to me", function () {
      movingObject.goal = new Goal(GoalType.REACH_POSITION);
      movingObject.goal.position = new Coordinate(90, 95);

      const actualDistance = Math.round(movingObject.getDistanceToGoal());
      assert.equal(actualDistance, 11);
    });
  });
});